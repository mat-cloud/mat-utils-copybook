package org.mat.copybook.data;

import org.mat.copybook.constant.LayoutConstants;

/**
 * @ClassName: LayoutConfig
 * @Date: 2022/9/19
 * @author: xinhe65045
 * @Version: 1.0
 * @Description: TODO
 */
public class LayoutConfig {

    /**
     * 纸张规格：
     * 1 = A4
     * 2 = A3
     * ...
     */
    private Integer paperSize;

    /**
     * 每页分区数
     */
    private Integer areasPerPage;

    /**
     * 每行单元格数
     */
    private Integer columnsPerRow;

    private Integer areaHeight;

    public Integer getPaperSize() {
        return paperSize;
    }

    public void setPaperSize(Integer paperSize) {
        this.paperSize = paperSize;
    }

    public Integer getAreasPerPage() {
        return areasPerPage;
    }

    public void setAreasPerPage(Integer areasPerPage) {
        this.areasPerPage = areasPerPage;
    }

    public Integer getColumnsPerRow() {
        return columnsPerRow;
    }

    public void setColumnsPerRow(Integer columnsPerRow) {
        this.columnsPerRow = columnsPerRow;
    }

    public Integer getAreaHeight() {
        return areaHeight;
    }

    public void setAreaHeight(Integer areaHeight) {
        this.areaHeight = areaHeight;
    }
}
