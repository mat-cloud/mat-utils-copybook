package org.mat.copybook.data;

import java.util.List;

/**
 * @ClassName: Page
 * @Date: 2022/9/19
 * @author: xinhe65045
 * @Version: 1.0
 * @Description: TODO
 */
public class Page {

    private List<String> pageContent;

    public List<String> getPageContent() {
        return pageContent;
    }

    public void setPageContent(List<String> pageContent) {
        this.pageContent = pageContent;
    }
}
