package org.mat.copybook.data;

import java.util.List;

/**
 * @ClassName: Copybook
 * @Date: 2022/9/18
 * @author: xinhe65045
 * @Version: 1.0
 * @Description: 字帖类
 * <p>
 * TODO 封装字帖核心属性
 */
public class Copybook {

    /**
     * 布局参数
     */
    private LayoutConfig layoutConfig;

    /**
     * 页眉信息
     */
    private Header header;

    /**
     * 页脚信息
     */
    private Footer footer;

    /**
     * 文本内容
     */
    private String content;

    /**
     * 分页
     */
    private List<Page> pageList;

    public LayoutConfig getLayoutConfig() {
        return layoutConfig;
    }

    public void setLayoutConfig(LayoutConfig layoutConfig) {
        this.layoutConfig = layoutConfig;
    }

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public Footer getFooter() {
        return footer;
    }

    public void setFooter(Footer footer) {
        this.footer = footer;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<Page> getPageList() {
        return pageList;
    }

    public void setPageList(List<Page> pageList) {
        this.pageList = pageList;
    }

    @Override
    public String toString() {
        return "Copybook{" +
                "layoutConfig=" + layoutConfig +
                ", header=" + header +
                ", footer=" + footer +
                ", content=" + content +
                ", pageList=" + pageList +
                '}';
    }
}
