package org.mat.copybook.layout;

import org.mat.copybook.constant.LayoutConstants;
import org.mat.copybook.data.Copybook;
import org.mat.copybook.data.LayoutConfig;
import org.mat.copybook.data.Page;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @ClassName: DefaultLayouter
 * @Date: 2022/9/19
 * @author: xinhe65045
 * @Version: 1.0
 * @Description: 默认布局器
 * <p>
 * TODO 根据文本内容和字帖布局配置，生成页眉、页脚、分页列表
 * <p>
 * TODO 支持动态传入系统默认配置
 */
public class DefaultLayouter {

    public static void calculationLayout(Copybook copybook) {
        //TODO 计算每页内容
        // 页面大小，按A4默认
        // 页面缩进，默认100
        // 页眉页脚，先写死，各留200
        // 布局方式，默认拼音（含笔顺）加田字格，每行一个字
        // 单元格大小，默认拼音80，田字格180
        // 分区缩进，默认下方缩进20？拼音缩进10？

        //TODO 填充pageList

        //region 计算每页内容
        // 页面大小
        Integer pageWidth;
        Integer pageHeight;

        LayoutConfig layoutConfig = copybook.getLayoutConfig();
        Integer paperSize = Optional.ofNullable(layoutConfig.getPaperSize()).orElse(1);
        switch (paperSize) {
            case 1:
                // 1、A4
                pageWidth = LayoutConstants.PaperSize.WIDTH_A4;
                pageHeight = LayoutConstants.PaperSize.HEIGHT_A4;
                break;
            case 2:
                // 2、A3
                pageWidth = LayoutConstants.PaperSize.WIDTH_A3;
                pageHeight = LayoutConstants.PaperSize.HEIGHT_A3;
                break;
            default:
                pageWidth = LayoutConstants.PaperSize.WIDTH_A4;
                pageHeight = LayoutConstants.PaperSize.HEIGHT_A4;
                System.out.println("默认使用A4");
        }

        // 计算分区高度
        //TODO 先默认布局方式，后续读取配置
        Integer bodyWidth = pageWidth - 100 * 2;
        Integer bodyHeight = pageHeight - 100 * 2 - 200 - 200;

        Integer areaWidth = bodyWidth;
        Integer areaHeight = LayoutConstants.CellSize.CELL_HEIGHT_PINYIN + LayoutConstants.CellSize.CELL_HEIGHT_TIAN + 20;
        layoutConfig.setAreaHeight(areaHeight);

        Integer areasPerPage = Math.floorDiv(bodyHeight, areaHeight);


        // 默认区域左右缩进0
        Integer rowWidth = areaWidth - 0;

        Integer columnsPerRow = Math.floorDiv(rowWidth,LayoutConstants.CellSize.CELL_WIDTH);


        // 保存配置，用于渲染
        layoutConfig.setAreasPerPage(areasPerPage);
        layoutConfig.setColumnsPerRow(columnsPerRow);
        //endregion

        //TODO 是否去空格？换行怎么处理？
        String content = Optional.ofNullable(copybook.getContent()).orElse("");
        char[] chars = content.toCharArray();
        List<String> stringList = new ArrayList<>();
        for (char aChar : chars) {
            stringList.add(String.valueOf(aChar));
        }
        Integer pageNum  = stringList.size() / areasPerPage + (stringList.size() % areasPerPage != 0 ? 1 : 0);
        List<Page> pageList = new ArrayList<>();
        for (int i = 0; i < pageNum; i++) {
            Integer startIndex = i*areasPerPage;
            Integer endIndex = (i+1)*areasPerPage;
            if (endIndex>stringList.size()){
                endIndex = stringList.size();
            }
            List<String> pageContent = stringList.subList(startIndex, endIndex);
            Page page = new Page();
            page.setPageContent(pageContent);
            pageList.add(page);
        }
        copybook.setPageList(pageList);



        //TODO

    }
}
