package org.mat.copybook.constant;

/**
 * @ClassName: SizeEnum
 * @Date: 2022/9/18
 * @author: xinhe65045
 * @Version: 1.0
 * @Description: 常用尺寸定义
 * <p>
 * TODO 这个讲道理可以去掉
 */
public enum SizeEnum {
    //A4纸张的宽度
    A4_WIDTH("A4_WIDTH", 2480),
    //A4纸张的高度
    A4_HEIGHT("A4_HEIGHT", 3508),
    //单元格的默认尺寸
    CELL_WIDTH("CELL_WIDTH", 180),
    CELL_HEIGHT("CELL_HEIGHT", 180),
    //拼音格的牧人尺寸
    PINYIN_WIDTH("PINYIN_WIDTH", 180),
    PINYIN_HEIGHT("PINYIN_HEIGHT", 80),
    ;

    private String code;
    private Integer value;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    SizeEnum(String code, Integer value) {
        this.code = code;
        this.value = value;
    }
}
