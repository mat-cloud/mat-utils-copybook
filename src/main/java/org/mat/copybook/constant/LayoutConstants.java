package org.mat.copybook.constant;

/**
 * @ClassName: LayoutConstants
 * @Date: 2022/9/19
 * @author: xinhe65045
 * @Version: 1.0
 * @Description: TODO
 */
public interface LayoutConstants {

    interface PaperSize {
        Integer WIDTH_A4 = 2480;
        Integer HEIGHT_A4 = 3508;

        Integer WIDTH_A3 = 2480;
        Integer HEIGHT_A3 = 3508;
    }

    interface CellSize {
        Integer CELL_WIDTH = 180;

        Integer CELL_HEIGHT_TIAN = 180;

        Integer CELL_HEIGHT_PINYIN = 80;

    }

}
