package org.mat.copybook.constant;

import java.awt.*;

/**
 * @ClassName: CopybookStyle
 * @Date: 2022/9/18
 * @author: xinhe65045
 * @Version: 1.0
 * @Description: 字帖样式常量
 */
public class CopybookStyle {
    /**
     * 是否显示拼音
     */
    public static boolean showPinyin = false;
    /**
     * 文字字体
     */
    public static Font font = new Font("宋体", Font.PLAIN, 140);
    /**
     * 拼音的字体
     */
    public static Font pinyinFont = new Font("黑体", Font.PLAIN, 50);
    /**
     * 文字颜色
     */
    public static Color textColor = Color.BLACK;
    /**
     * 临摹的文字颜色
     */
    public static Color copyTextColor = Color.BLACK;

    /**
     * 模板宽度
     */
    public static Integer width = SizeEnum.A4_WIDTH.getValue();
    /**
     * 模板高度
     */
    public static Integer height = SizeEnum.A4_HEIGHT.getValue();
}
